from tkinter.messagebox import *
from tkinter import *
import string
import random
import sys
import time
import threading

cols = 20
rows = 10
taille_cells = 35
afforestation = 1


def initialisation_tableau() :
            global list_complete
            list_complete = list()
            
            posx = 0
            posy = 0
            for ligne in range(rows):

                for colonne in range(cols):
                    
                    alea = random.randint(0, 100)
                    if alea <= afforestation * 100: 
                        etat = "green"
                    else:
                        etat = "white"

                    list_complete.append(canvas.create_rectangle( posx, posy, posx + taille_cells, posy + taille_cells, fill=etat))
                    posx = posx + taille_cells

                posy = posy + taille_cells
                posx = 0




def propager (ligne, colonne): 
    voisin_vert = 0
    if ligne - 1 != -1 :
        if canvas.itemcget(list_complete[(ligne-1)*cols+colonne], "fill") == "gray" :
            voisin_vert = voisin_vert + 1
  
    if colonne - 1 != -1 :
        if canvas.itemcget(list_complete[ligne*cols+(colonne-1)], "fill") == "gray" :
            voisin_vert = voisin_vert + 1

    if colonne + 1 != cols :
        if canvas.itemcget(list_complete[ligne*cols+(colonne+1)], "fill") == "red" :
            voisin_vert = voisin_vert + 1

    if  ligne + 1 != rows :
        if canvas.itemcget(list_complete[(ligne+1)*cols+colonne], "fill") == "red" :
            voisin_vert = voisin_vert + 1

    if voisins == True:
        if ligne - 1 != -1 and colonne + 1 != cols :
            if canvas.itemcget(list_complete[(ligne-1)*cols+(colonne+1)], "fill") == "gray" :
                voisin_vert = voisin_vert + 1
        if colonne - 1 != -1 and ligne + 1 != rows:
            if canvas.itemcget(list_complete[(ligne+1)*cols+(colonne-1)], "fill") == "red" :
                voisin_vert = voisin_vert + 1
        if colonne - 1 != -1 and ligne - 1 != -1 :
            if canvas.itemcget(list_complete[(ligne-1)*cols+(colonne-1)], "fill") == "gray" :
                voisin_vert = voisin_vert + 1
        if  ligne + 1 != rows and colonne + 1 != cols:
            if canvas.itemcget(list_complete[(ligne+1)*cols+(colonne+1)], "fill") == "red" :
                voisin_vert = voisin_vert + 1

    return voisin_vert

def voisin_feu(ligne, colonne):
    crame = False
    voisin_vert = propager(ligne, colonne)    
    
    if t ==True:
        if voisin_vert > 0:
            crame = True
    else:
        alea = random.randint(0, 1)
        if alea < (1-(1/(voisin_vert+1))):
            crame=True

    return crame


def incendie():
    for ligne in range(rows):
        for colonne in range(cols):
            couleur = canvas.itemcget(list_complete[ligne*cols+colonne], "fill")
            crame = False
            if couleur == "green" :
                crame = voisin_feu(ligne, colonne)
                
            if  crame  and couleur == "green" or couleur != "white" and couleur != "green" :
               changer_etat_cell(ligne, colonne, crame)
    if crame == False and mode == False:
        root.after(1000, incendie)

def bouton_pasapas():
    global mode
    mode = True
    incendie()


def bouton_simu():
    global mode
    mode = False
    incendie()

def bouton_moore():
    global voisins
    voisins = True


def bouton_von():
    global voisins
    voisins = False
def bouton_r1():
    global t
    t=True

def bouton_r2():
    global t
    t=False

def click_callback(event):
    if event.num == 1 or event.num == 3:
        x1 = (int)(event.x/taille_cells)
        y1 = (int)(event.y/taille_cells)
        if canvas.itemcget(list_complete[y1*cols+x1], "fill") == "green":
            canvas.itemconfig(list_complete[y1*cols+x1], fill='red')
        

def changer_etat_cell(ligne, colonne, crame):
    couleur_actuelle = canvas.itemcget(list_complete[ligne*cols+colonne], "fill")
    
    if couleur_actuelle == "gray":
        nouvelle_couleur = "white"
    elif couleur_actuelle == "red":
        nouvelle_couleur = "gray"
    elif couleur_actuelle == "green" and crame:
        nouvelle_couleur = "red"
    elif couleur_actuelle == "green" and not crame:
        nouvelle_couleur = "green"
    
    if couleur_actuelle != nouvelle_couleur:
        canvas.itemconfig(list_complete[ligne*cols+colonne], fill=nouvelle_couleur)




root = Tk()
root.title("Feux de Foret")



canvas = Canvas(root, width =cols * taille_cells, height = rows * taille_cells)
canvas.grid(row=1, padx=30, pady=30, sticky='news')
canvas.pack()



initialisation_tableau()
canvas.bind('<Button-1>', click_callback)
canvas.bind('<Button-3>', click_callback)

bouton_pas_a_pas=Button(root, text="Pas Ã  pas", command=bouton_pasapas)
bouton_pas_a_pas.pack()

bouton_simulation=Button(root, text="Simulation", command=bouton_simu)
bouton_simulation.pack()

bouton_reset=Button(root, text="Recommencer", command=initialisation_tableau)
bouton_reset.pack()

bouton_moore=Button(root, text="Moore", command=bouton_moore)
bouton_moore.pack()

bouton_von=Button(root, text="Von  Neumann", command=bouton_von)
bouton_von.pack()

bouton_r1=Button(root, text="RÃ¨gle nÂ°1", command=bouton_r1)
bouton_r1.pack()

bouton_r2=Button(root, text="RÃ¨gle nÂ°2", command=bouton_r2)
bouton_r2.pack()    


root.mainloop()
